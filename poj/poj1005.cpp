#include <iostream>
const double PI=3.1415926;
using namespace std;
int main() {
  int n,i;
  double x,y,r;
  cin>>n;
  cin>>x>>y;
  for (i=1;i<=n;i++) {
    cin>>x>>y;
    for (r=1;x*x+y*y>100*r/PI;r++);
    cout<<"Property "<<i<<": This property will begin eroding in year "<<r<<"."<<endl;
  }
  cout<<"END OF OUTPUT."<<endl;
}
