#include <iostream>
#include <cmath>
using namespace std;
bool check(int x) {
  static int i;
  for(i=3;i<=int(sqrt(double(x)));i++)
    if (x%i==0) return false;
  return true;
}
int main() {
  int n,i;
  cin>>n;
  while (n!=0) {
    for(i=2;i<n/2;i++)
      if ((check(i)) && (check(n-i))) { cout<<n<<" = "<<i<<" + "<<n-i<<endl; break; }
    cin>>n;
  }
  return 0;
}
