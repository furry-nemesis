const
	a:array [1..19] of string=('pop', 'no', 'zip', 'zotz', 'tzec', 'xul', 'yoxkin', 'mol', 'chen', 'yax', 
								   'zac', 'ceh', 'mac', 'kankin', 'muan', 'pax', 'koyab', 'cumhu','uayet');
	b:array [0..19] of string=('imix', 'ik', 'akbal', 'kan', 'chicchan', 'cimi', 'manik', 'lamat', 'muluk', 'ok', 
									'chuen', 'eb', 'ben', 'ix', 'mem', 'cib', 'caban', 'eznab', 'canac', 'ahau');

var 
	n,i,p,year,month,day,days,ansyear:longint;
	s:string;
	
function change1(s:string):longint;
var
	i:longint;
	
begin 
	for i:=1 to 19 do if s=a[i] then exit(i);
end;

begin  
	readln(n); writeln(n);
	for i:=1 to n do 
		begin
			readln(s);
			p:=pos('.',s);
			val(copy(s,1,p-1),day);
			delete(s,1,p+1);
			p:=pos(' ',s);
			month:=change1(copy(s,1,p-1));
			val(copy(s,p+1,length(s)),year);
			days:=day+1+(month-1)*20+year*365;
			ansyear:=(days-1) div 260;
			dec(days,ansyear*260);
			writeln((days-1) mod 13+1,' ',b[(days-1) mod 20],' ',ansyear);
		end;
end.