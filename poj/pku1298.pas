const
a:array ['A'..'Z'] of char=('V','W','X','Y','Z','A','B','C','D','E','F',
			    'G','H','I','J','K','L','M','N','O','P','Q',
			    'R','S','T','U');

var
   s : string;
   i : longint;
begin
   repeat
      readln(s);
      if s='ENDOFINPUT' then break;
      readln(s);
      for i:=1 to length(s) do
	 if s[i] in ['A'..'Z'] then write(a[s[i]]) else write(s[i]);
      writeln;
      readln(s);
   until false;
end.
