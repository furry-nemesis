#include <iostream>
#include <string>
#include <math.h>
#include <fstream>
#define PI 3.141592653

class Complex{
public:
Complex();                                        //Default constructor~Complex();//Default destructor.
void Set(double new_real,double new_imaginary);    //Set data members.
double Real();                                    //Return the real part.
double Imaginary();//Return the imaginary part.
Complex operator+(Complex);//Overloaded+operator
Complex operator*(Complex);//Overloaded*operator
Complex operator=(Complex);//Overloaded=operator
int operator==(Complex);//Overloaded==operator
private:
double real;
double imaginary;
};


class Qubit{
public:
Qubit();//Default constructor.
~Qubit();//Default destructor.
int Measure();//Returns zero_state=0 or one_state=1 in accordanc
//with the probabilities of zero_state and one_state
void Dump();//Prints our zero_state,and one_state,without
//disturbing anything,this operation has no physic
//realization,it is only for information and debugin
//It should never be used in an algorithm for
//information.
void SetState(Complex zero_prob,Complex one_prob);
//Takes two complex numbers and sets the states t
//those values.
void SetAverage();//Sets the state to 2^(1/2)zero_state,2^(1/2)
//one_state.No imaginary/phase component.
double MCC(int state);//Multiply the zero or one state by its complex
//conjugate,and return the value.This valu
//will always be a real number,with no imagi
//component.
private:
Complex zero_state;
//The probability of finding the Qubit in the zero or all imarinary
//state.I currently use only the real portion.
Complex one_state;
//The probability of finding the Qubit in the one or all real state.//I currently use only the real portion.
//|zero_state|^2+|one_state|^2 should always be 1.
//This notation means z_s*ComplexConjugate(z_s)+o_s*
//ComplexConjugate(o_s)=1.
};

class QuReg{
public:
QuReg(int size);
QuReg();//Default Constructor
QuReg(const QuReg&);//Copy constructor
~QuReg();//Default destructor.
int DecMeasure();//Measures our quantum register,and returns the
void Dump(int verbose);
void SetState(Complex new_state[]);
void SetAverage(int number);
void Norm();
Complex GetProb(int state);
//Return the size of the register.
int Size();
private:
int reg_size;
Complex*State;
};

int GetQ(int n)
{
int power=8;//265 is the smallest q ever is.
while(pow(2,power)<pow(n,2))
{
power=power+1;
}
return((int)pow(2,power));
}



int modexp(int x,int a,int n)
{
int value=1;int tmp;
tmp=x%n;
while(a>0)
{
if(a&1)
{
value=(value*tmp)%n;
}
tmp=tmp*tmp%n;
a=a>>1;
}
return value;
}


int RegSize(int a){
int size=0;
while(a!=0){
a=a>>1;
size++;
}
return(size);
}



int Max(int a,int b)
{
return(a>b?a:b);
}



int GCD(int a,int b)
{
int d;
if(b!=0)
{
while(a%b!=0)
{
d=a%b;
a=b;
b=d;
}
}
else
{
return-1;
}
return(b);
}


void DFT(QuReg*reg,int q)
{
Complex*init=new Complex[q];
Complex tmpcomp;
tmpcomp.Set(0,0);
int count=0;
double tmpreal=0;
double tmpimag=0;
double tmpprob=0;
for(int a=0;a<q;a++){
if((pow(reg->GetProb(a).Real(),2)+pow(reg->GetProb(a).Imaginary(),2))>pow(10,-14))
{
for(int c=0;c<q;c++)
{
tmpcomp.Set(pow(q,-.5)*cos(2*PI*a*c/q),pow(q,-.5)*sin(2*PI*a*c/q));
init[c]=init[c]+(reg->GetProb(a)*tmpcomp);
}
}
count++;
if(count==100)
{


cout<<"Making progress in Fourier transform,"<<100*((double)a/(double)(q-1))
<<"%done!"<<endl<<flush;
count=0;
}
}
reg->SetState(init);
reg->Norm();
delete[]init;
}