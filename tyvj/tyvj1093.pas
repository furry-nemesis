var
a:array [1..9,1..9] of longint;
b:array [1..9] of boolean;
i,j,k,l,p,n:longint;
f:boolean;

begin
   readln(n);
   for p:=1 to n do
   begin
      f:=true;
      for i:=1 to 9 do
	 for j:=1 to 9 do
	    read(a[i,j]);
      for i:=1 to 9 do
      begin
	 fillchar(b,sizeof(b),false);
	 for j:=1 to 9 do
	    if b[a[i,j]]=false then b[a[i,j]]:=true else begin f:=false; break; end;
	 if not f then break;
	 fillchar(b,sizeof(b),false);
	 for j:=1 to 9 do
	    if b[a[j,i]]=false then b[a[j,i]]:=true else begin f:=false; break; end;
	 if not f then break;
      end;
      if not f then begin writeln('Wrong'); continue; end;
      for i:=1 to 3 do
      for j:=1 to 3 do
      begin
	 fillchar(b,sizeof(b),false);
	 for k:=(i-1)*3+1 to i*3 do
	    for l:=(j-1)*3+1 to j*3 do
	       if b[a[k,l]]=false then b[a[k,l]]:=true else begin f:=false; break; end;
	if not f then break;
      end;
      if not f then begin writeln('Wrong'); continue; end;
      writeln('Right');
   end;
end.
