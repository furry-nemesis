var
n,m,i,j : longint;
a       : array [0..5000,1..2] of longint;

procedure qsort(l,r : longint);
var
   i,j,m : longint;

begin
   i:=l; j:=r;
   m:=(l+r) div 2;
   repeat
      while (a[i,2]>a[m,2]) or ((a[i,2]=a[m,2]) and (a[i,1]<a[m,1])) do inc(i);
      while (a[j,2]<a[m,2]) or ((a[j,2]=a[m,2]) and (a[j,1]>a[m,1])) do dec(j);
      if i<=j then
      begin
	 a[0]:=a[i]; a[i]:=a[j]; a[j]:=a[0];
	 inc(i); dec(j);
      end;
   until i>j;
   if l<j then qsort(l,j);
   if r>i then qsort(i,r);
end; { qsort }

procedure sort(l,r : longint);
var
   i,j : longint;

begin
   for i:=l to r-1 do
      for j:=i+1 to r do
	 if a[i,1]>a[j,1] then
	 begin
	    a[0]:=a[i]; a[i]:=a[j]; a[j]:=a[0];
	 end;
end; { sort }

begin
   readln(n,m);
   for i:=1 to n do
      readln(a[i,1],a[i,2]);
   qsort(1,n);
   m:=trunc(m*1.5);
   write(a[m,2],' ');
   j:=1;
   while (j<=n) and (a[j,2]>=a[m,2]) do inc(j);
   dec(j);
   writeln(j);
   for i:=1 to j do writeln(a[i,1],' ',a[i,2]);
end. { main }
