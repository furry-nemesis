type
arr=array [0..2000] of longint;

var
   n,m,i,j,ans : longint;
   a,b         : arr;

procedure qsort(l,r : longint; var a:arr);
var
   i,j,m : longint;

begin
   i:=l; j:=r;
   m:=a[(l+r) div 2];
   repeat
      while a[i]>m do inc(i);
      while a[j]<m do dec(j);
      if i<=j then
      begin
	 a[0]:=a[i]; a[i]:=a[j]; a[j]:=a[0];
	 inc(i); dec(j);
      end;
   until i>j;
   if l<j then qsort(l,j,a);
   if r>i then qsort(i,r,a);
end; { qsort }

begin
   readln(n,m);
   for i:=1 to n do readln(a[i]);
   for i:=1 to m do begin readln(b[i]); if b[i]=0 then b[i]:=301; end;
   qsort(1,n,a);
   qsort(1,m,b);
   ans:=m;
   for i:=1 to n do
      for j:=1 to m do
	 if a[i]>b[j] then
	 begin
	    b[j]:=301;
	    dec(ans);
	    break;
	 end;
   writeln(ans);
end.
