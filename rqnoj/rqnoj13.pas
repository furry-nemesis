var
l,m,i,j,ans,p,q:longint;
a:array [0..10000] of boolean;

begin
   readln(l,m);
   for i:=1 to m do
   begin
      readln(p,q);
      for j:=p to q do a[j]:=true;
   end;
   for i:=0 to l do
      if not a[i] then inc(ans);
   writeln(ans);
end.
