var
  a:array [0..26] of longint;
  s,t,w,i,j,k,p,q:longint;
  ch:char;

begin
  readln(s,t,w);
  q:=t-s+1; p:=1;
  for i:=1 to w do
    begin
      read(ch);
      a[i]:=ord(ch)-ord('a')-s+2;
    end;
  for i:=1 to 5 do
    begin
      j:=w;
      while (j>0) and (a[j]=t-w+j) do dec(j);
      if j=0 then break;
      inc(a[j]);
      for k:=j+1 to w do a[k]:=a[j]+k-j;
      for k:=1 to w do write(chr(ord('a')+a[k]+s-2));
       writeln;
    end;
end.