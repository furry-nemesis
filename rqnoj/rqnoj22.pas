var
n,k,i,j,l:longint;
a:array [0..10000] of longint;

begin
   readln(n,k);
   for i:=1 to n do read(a[i]);
   for i:=1 to k do
   begin
      j:=n-1;
      while (j>0) and (a[j]>a[j+1]) do dec(j);
      if j=0 then
      begin
	 for j:=1 to n do a[j]:=j;
	 continue;
      end;
      for l:=n downto j+1 do
	 if (a[l]>a[j]) then break;
      a[0]:=a[j]; a[j]:=a[l]; a[l]:=a[0];
      for l:=j+1 to (n+j+1) div 2 do
      begin
	 a[0]:=a[l]; a[l]:=a[n-l+j+1]; a[n-l+j+1]:=a[0];
      end;
   end;
   write(a[1]);
   for i:=2 to n do write(' ',a[i]);
end.
