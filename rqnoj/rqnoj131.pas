var
n,i,j:longint;
hash:array [1..1000000] of boolean;

begin
   readln(n);
   fillchar(hash,sizeof(hash),true);
   for i:=2 to trunc(sqrt(n)) do
      for j:=2 to n div i do hash[i*j]:=false;
   for i:=4 to n do
   if i mod 2=0 then
   begin
      for j:=2 to i div 2+1 do
	 if (hash[j]) and (hash[i-j]) then break;
      writeln(i,'=',j,'+',i-j);
   end;
end.
