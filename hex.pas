var
  i,n,k:longint;
  hex:string;

procedure jian(var hex:string; k:longint);
var
  i,j:longint;
  s:set of char;

begin
  s:=[];
  for i:=1 to k-1 do s:=s+[hex[i]];
  for i:=k to length(hex) do
    begin
      if i>k then hex[i]:=succ('F');
      repeat
        hex[i]:=pred(hex[i]);
        if hex[i]=pred('A') then hex[i]:='9';
      until not(hex[i] in s);
      s:=s+[hex[i]];
    end;
end;

begin
  readln(n);
  hex:='FEDCBA98';
  while n>1 do
    begin
      k:=1;
      for i:=1 to length(hex)-1 do k:=k*(16-i);
      i:=1;
      repeat
        if n>k then break;
        k:=k div (16-i);
        inc(i);
      until false;
      jian(hex,i);
      dec(n,k);
      if hex[1]='0' then delete(hex,1,1);
    end;
  writeln(hex);
end.
