var
  br,gr,cl:array [0..3] of longint;
  i,a,b,c,ans,t:longint;
  minway:string;

function trans(a:longint):char;
begin
  case a of
    1:exit('B');
    2:exit('C');
    3:exit('G');
  end;
end;

begin
  for i:=1 to 3 do
    readln(br[i],gr[i],cl[i]);
  ans:=maxlongint;
  for a:=1 to 3 do
    for b:=1 to 3 do
      if (a<>b) then
        for c:=1 to 3 do
          if (b<>c) and (a<>c) then
            begin
              t:=br[b]+br[c]+cl[a]+cl[c]+gr[a]+gr[b];
              if t<ans then
                begin
                  ans:=t;
                  minway:=trans(a)+trans(b)+trans(c);
                end;
            end;
  writeln(minway,' ',ans);
end.