type
  leavetype=array [0..500] of boolean;

var
  p,q,n,min,i:longint;
  know:array [0..500,0..500] of longint;
  leave,lea:leavetype;

function deg(i:longint):longint;
var
  j:longint;

begin
  deg:=0;
  for j:=1 to n do
    if (not lea[j]) and (know[i,j]=1) then inc(deg);
end;

function connect(i:longint):longint;
var
  j:longint;

begin
  for j:=1 to n do if (not lea[j]) and (know[i,j]=1) then break;
  exit(j);
end;

function noknow:boolean;
var
  i,j:longint;

begin
  noknow:=true;
  for i:=1 to n-1 do
    for j:=i+1 to n do
      if (know[i,j]=1) and (not (lea[i])) and (not (lea[j])) then exit(false);
end;

function pass(i:longint):boolean;
begin
  pass:=true;
  if (lea[i]) or (deg(i)=0) then exit(false);
end;

procedure save(p:longint);
begin
  leave:=lea;
  min:=p;
end;

function done(i:longint):boolean;
var
  j:longint;

begin
  done:=true;
  for j:=1 to n do
    if j<>i then
      if (know[i,j]=1) and (deg(j)=2) then
        if connect(j)<i then exit;
  done:=false;
end;

procedure dfs(s:longint);
var
  leabak:^leavetype;
  i,j,k:longint;

begin
  if s>min then exit;
  if noknow then begin save(s-1); exit; end;
  new(leabak);
  leabak^:=lea; k:=0;
  i:=0;
  repeat
    inc(i);
    if lea[i] then continue;
    j:=deg(i);
    if j=1 then
      begin
        i:=connect(i);
        inc(k);
        lea[i]:=true;
        i:=0;
      end
    else if j>(n-s-k+1) div 2 then
      begin
        inc(k);
        lea[i]:=true;
        i:=0;
      end;
  until i=n;
  if k>0 then
    begin
      dfs(s+k);
      lea:=leabak^;
    end
  else
    for i:=1 to n do
      if pass(i) and not done(i) then
        begin
          lea[i]:=true;
          dfs(s+1);
          lea[i]:=false;
        end;
  dispose(leabak);
end;


begin
  readln(n);
  repeat
    readln(p,q);
    know[p,q]:=1; know[q,p]:=1;
  until (p=0) and (q=0);
  min:=n;
  dfs(1);
  writeln(min);
  for p:=1 to n do if leave[p] then write(p,' ');
end.
