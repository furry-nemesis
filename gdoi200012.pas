type
	arr=array [-5000..5000] of longint;

var
	a,b:array [-5000..5000] of longint;
	domino:array [0..1000] of longint;
	i,j,p,q,n:longint;

function min(a,b:longint):longint;
begin
	if a<b then exit(a) else exit(b);
end;

procedure swap(var a,b:arr);
var
	t:arr;

begin
	t:=a; a:=b; b:=t;
end;

begin
	readln(n);
	for i:=1 to n do 
		begin
			readln(p,q);
			domino[i]:=p-q;
		end;
	for i:=-5000 to 5000 do begin a[i]:=maxint; b[i]:=maxint; end;
	p:=0; q:=0; a[0]:=0;
	for i:=1 to n do 
		begin
			for j:=p to q do 
				if a[j]<maxint then
					begin
						b[j+domino[i]]:=min(a[j],b[j+domino[i]]);
						b[j-domino[i]]:=min(a[j]+1,b[j-domino[i]]);
						a[j]:=maxint;
					end;
			p:=p-abs(domino[i]);
			q:=q+abs(domino[i]);
			swap(a,b);
		end;
	for i:=0 to n do 
		if (a[i]<>maxint) or (a[-i]<>maxint) then break;
	writeln(min(a[i],a[-i]));
end.